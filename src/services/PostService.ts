import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { PostInterface } from '../types/PostInterface';

export const postAPI = createApi({
  reducerPath: 'postAPI',

  baseQuery: fetchBaseQuery({
    baseUrl: ' http://localhost:5000',
  }),

  tagTypes: ['Post'],

  endpoints: (build) => ({
    fetchAllPosts: build.query<PostInterface[], number>({
      query: (limit = 5) => ({
        url: '/posts',
        params: {
          _limit: limit,
        },
      }),
      providesTags: (result) => ['Post'],
    }),

    createPost: build.mutation<PostInterface, PostInterface>({
      query: (post) => ({
        url: '/posts',
        method: 'POST',
        body: post,
      }),
      invalidatesTags: ['Post'],
    }),

    updatePost: build.mutation<PostInterface, PostInterface>({
      query: (post) => (
        console.log(post),
        {
          url: `/posts/${post.id}`,
          method: 'PUT',
          body: post,
        }
      ),
      invalidatesTags: ['Post'],
    }),

    deletePost: build.mutation<PostInterface, PostInterface>({
      query: (post) => ({
        url: `/posts/${post.id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Post'],
    }),
  }),
});

export const {
  useFetchAllPostsQuery,
  useCreatePostMutation,
  useUpdatePostMutation,
  useDeletePostMutation,
} = postAPI;
