import { FC, MouseEvent } from 'react';
import {
  useCreatePostMutation,
  useDeletePostMutation,
  useFetchAllPostsQuery,
  useUpdatePostMutation,
} from '../../services/PostService';
import { PostInterface } from '../../types/PostInterface';
import { PostProps } from './Post.props';

export const Post: FC<PostProps> = () => {
  const { data: posts, error, isLoading } = useFetchAllPostsQuery(100);
  const [createPost, {}] = useCreatePostMutation();
  const [updatePost, {}] = useUpdatePostMutation();
  const [deletePost, {}] = useDeletePostMutation();

  const handleCreate = () => {
    const title = prompt('Title', 'Test Title');

    if (title) {
      createPost({ title, body: title } as PostInterface);
    }
  };

  const handleUpdate = (post: PostInterface) => {
    const title = prompt('', post.title) || post.title;

    updatePost({ ...post, title } as PostInterface);
  };

  const handleDelete = (e: MouseEvent, post: PostInterface) => {
    e.stopPropagation();

    deletePost(post);
  };

  return (
    <>
      {isLoading && <h2>Идёт загрузка...</h2>}
      {error && <h2>Ошибка!!!</h2>}
      <button onClick={handleCreate}>Add new Post</button>

      <ul>
        {posts?.map((post) => (
          <li
            key={post.id}
            style={{ border: '1px solid black', padding: '10px' }}
          >
            <span>{post.id}</span>){' '}
            <b onClick={() => handleUpdate(post)}>{post.title}</b>
            <button onClick={(e) => handleDelete(e, post)}>Delete</button>
          </li>
        ))}
      </ul>
    </>
  );
};
