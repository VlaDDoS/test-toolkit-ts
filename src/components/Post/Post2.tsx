import { FC } from 'react';
import { useFetchAllPostsQuery } from '../../services/PostService';
import { PostProps } from './Post.props';

export const Post2: FC<PostProps> = () => {
  const { data: posts, error, isLoading } = useFetchAllPostsQuery(100);

  return (
    <>
      {isLoading && <h2>Идёт загрузка...</h2>}
      {error && <h2>Ошибка!!!</h2>}

      <ul>
        {posts?.map((post) => (
          <li
            key={post.id}
            style={{ border: '1px solid black', padding: '10px' }}
          >
            <span>{post.id}</span>) <b>{post.title}</b>
            <button>Delete</button>
          </li>
        ))}
      </ul>
    </>
  );
};
