import React, { FC, useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../../hooks/useTypedRedux';
import { fetchUsers } from '../../store/User/UserActionCreators';
import { UserProps } from './User.props';

export const User: FC<UserProps> = () => {
  const { users, isLoading, errors } = useAppSelector(
    (state) => state.userReducer
  );
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchUsers());
  }, []);

  return (
    <>
      {isLoading && <h2>Идёт загрузка...</h2>}
      {errors && <h2>{errors}</h2>}
      <ul>
        {users.map((user) => (
          <li key={user.id}>{user.name}</li>
        ))}
      </ul>
    </>
  );
};
