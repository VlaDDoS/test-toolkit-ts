import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { UserInterface } from '../../types/UserInterface';
import { fetchUsers } from './UserActionCreators';

interface UserState {
  users: UserInterface[];
  isLoading: boolean;
  errors: string | null;
}

const initialState: UserState = {
  users: [],
  isLoading: false,
  errors: null,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    userFetching(state) {
      state.isLoading = true;
    },
    userFetchingSuccess(state, action: PayloadAction<UserInterface[]>) {
      state.isLoading = false;
      state.errors = null;
      state.users = action.payload;
    },
    userFetchingError(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.users = [];
      state.errors = action.payload;
    },
  },
});

export const userReducer = userSlice.reducer;
