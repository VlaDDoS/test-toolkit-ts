import axios from 'axios';
import { AppDispatch } from '..';
import { UserInterface } from '../../types/UserInterface';
import { userSlice } from './UserSlice';

export const fetchUsers = () => {
  const { userFetching, userFetchingError, userFetchingSuccess } =
    userSlice.actions;

  return async (dispatch: AppDispatch) => {
    try {
      dispatch(userFetching());

      const response = await axios.get<UserInterface[]>(
        'https://jsonplaceholder.typicode.com/users'
      );

      dispatch(userFetchingSuccess(response.data));
    } catch (error) {
      if (error instanceof Error) {
        dispatch(
          userFetchingError(`Произошла ошибка при получении Users: ${error}`)
        );
      }
    }
  };
};
