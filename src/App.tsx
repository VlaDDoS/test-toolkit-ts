import { Post, Post2, User } from './components';

export const App = () => {
  return (
    <div className="App">
      <h1>Hello redux toolkit</h1>
      <h2>Users:</h2>
      <User />
      <hr />
      <h2>Posts:</h2>
      <div style={{ display: 'flex' }}>
        <div>
          <Post />
        </div>
        <div>
          <Post2 />
        </div>
      </div>
    </div>
  );
};
